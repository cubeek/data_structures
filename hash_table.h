#ifndef _HASH_TABLE_H
#define _HASH_TABLE_H

#include "list.h"

typedef struct SHashTable {
    int size;
    TList **table;
} THashTable;

THashTable *create_hashtable(int size);
void delete_table(THashTable **);
void hash_input(THashTable *, unsigned int (*)(int, char*), char *);
TList *lookup_list(THashTable *, unsigned int (*)(int, char*), char *);
void print_table(THashTable *);

unsigned int hash(int, char *);

#endif
