#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "sorts.h"

int get_random(int start, int end) {
    return rand() % end + start;
}

void generate_list(struct SList *list, int length) {
    int number;
    for (int i=0; i < length; i++) {
        number = get_random(0, 100);
        insert_last(list, number, NULL);
    }
}

int main(int argc, char *argv[]) {
    struct SList *list = NULL;
    struct SList *ordered = NULL;
    int length = 0;

    if (argc <= 1) {
        fprintf(stderr, "Give me a length of the list!\n");
        return EXIT_FAILURE;
    }

    length = atoi(argv[1]); // Check for errors!
    srand(time(NULL));
    init_list(&list);
    generate_list(list, length);
    print_list(list);

    printf("Insert sort\n");
    ordered = insert_sort(list);
    print_list(ordered);
    delete_list(&ordered);
    printf("-------------\n");

    printf("Select sort\n");
    ordered = select_sort(list);
    print_list(ordered);
    delete_list(&ordered);
    printf("-------------\n");

    printf("Quick sort\n");
    ordered = quick_sort(list);
    print_list(ordered);
    delete_list(&ordered);
    printf("-------------\n");

    printf("Merge sort\n");
    ordered = merge_sort(list);
    print_list(ordered);
    delete_list(&ordered);
    printf("-------------\n");



    delete_list(&list);
    return EXIT_SUCCESS;
}
