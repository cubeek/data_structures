#include <stdlib.h>
#include <stdio.h>

#include "hash_table.h"

int main(int argc, char *argv[]) {
    THashTable *table;
    char string1[] = "Kyblicek";
    char string2[] = "Pytlicek";
    char string3[] = "Kolicek";

    if (argc <= 1) {
        fprintf(stderr, "Need size of hashtable\n");
        return EXIT_FAILURE;
    }

    table = create_hashtable(atoi(argv[1]));
    hash_input(table, hash, string1);
    hash_input(table, hash, string2);
    hash_input(table, hash, string2);
    hash_input(table, hash, string3);

    print_table(table);

    delete_table(&table);
    return EXIT_SUCCESS;
}
