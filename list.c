#include <stdlib.h>
#include <stdio.h>

#include "list.h"

void init_list(struct SList **list) {
    if (*list != NULL) {
        return;
    }
    *list = (struct SList *)malloc(sizeof(struct SList));
    (*list)->first = NULL;
    (*list)->last = NULL;
    (*list)->length = 0;
}

void delete_list(struct SList **list) {
    if (*list == NULL) {
        return;
    }
    while ((*list)->first != NULL) {
        delete_first((*list));
    }
    free(*list);
    *list = NULL;
}

struct SElement *create_element(int data, char *str) {
    struct SElement *element = (struct SElement *)malloc(sizeof(struct SElement));
    element->data = data;
    element->str = str;
    element->next = NULL;
    element->prev = NULL;
    return element;
}

void delete_element(struct SElement *element) {
    if (element->str != NULL) {
        free(element->str);
    }
    free(element);
    element = NULL;
}

void insert_first(struct SList *list, int data, char *str) {
    struct SElement *element = create_element(data, str);
    if (list == NULL) {
        return;
    }
    if (list->first == NULL) {
        list->first = element;
        list->last = element;
    } else {
        element->next = list->first;
        list->first->prev = element;
        list->first = element;
    }
    list->length += 1;
}

void insert_last(struct SList *list, int data, char *str) {
   struct SElement *element = create_element(data, str);
   if (list == NULL) {
       return;
   }
   if (list->first == NULL) {
      list->first = element;
      list->last = element;
   } else {
      list->last->next = element;
      element->prev = list->last;
      list->last = element;
   }
   list->length += 1;
}

void delete_first(struct SList *list) {
   struct SElement *element;

   if (list->first == NULL) {
       return;
   }
   element = list->first;
   if (list->last == list->first) {
       list->last = NULL;
       list->first = NULL;
   } else {
       list->first = list->first->next;
       list->first->prev = NULL;
   }
   delete_element(element);
    list->length -= 1;
}

void delete_last(struct SList *list) {
   struct SElement *element;

   if (list->first == NULL) {
       return;
   }
   element = list->last;
   if (list->first == list->last) {
       list->last = NULL;
       list->first = NULL;
   } else {
       list->last = list->last->prev;
       list->last->next = NULL;
   }
   delete_element(element);
    list->length -= 1;
}

void insert(TList *list, TElement *act, TElement *element) {
   if (list == NULL) {
       return;
   }
    list->length += 1;
   if (list->first == NULL) {
       list->first = element;
       list->last = element;
       return;
   }
   if (list->first == act) {
       list->first = element;
   } else {
       act->prev->next = element;
       element->prev = act->prev;
   }
   element->next = act;
   act->prev = element;
}

void append(TList *list, TElement *act, TElement *element) {
   if (list == NULL) {
       return;
   }
   list->length += 1;
   if (list->first == NULL) {
       list->first = list->last = element;
       element->next = element->prev = NULL;
       return;
   }
   if (list->last == act) {
       list->last = element;
       element->next = NULL;
   } else {
       element->next = act->next;
       act->next->prev = element;
   }
   element->prev = act;
   act->next = element;
}

void delete(struct SList *list, struct SElement *act) {
   if (list == NULL) {
       return;
   }
    list->length -= 1;
   if (act == list->first) {
       list->first = act->next;
       if (list->first != NULL) {
           list->first->prev = NULL;
       }
   } else {
       act->prev->next = act->next;
   }
   if (act == list->last) {
       list->last = act->prev;
       if (list->last != NULL) {
           list->last->next = NULL;
       }
   } else {
       act->next->prev = act->prev;
   }

   act->next = act->prev = NULL;
}

void copy_list(struct SList *src, struct SList *dst) {
    for (struct SElement *act = src->first; act != NULL; act = act->next) {
        insert_last(dst, act->data, NULL);
    }
}

TList *merge_list_with_data(TList *first, TList *second, int data) {
    TList *result = NULL;
    TElement *act = NULL;

    init_list(&result);
    for(act = first->first; act != NULL; act = act->next) {
        insert_last(result, act->data, NULL);
    }
    insert_last(result, data, NULL);
    for(act = second->first; act != NULL; act = act->next) {
        insert_last(result, act->data, NULL);
    }

    return result;
}

void print_list(struct SList *list) {
   struct SElement *el = list->first;

   printf("List(%d): ", list->length);
   while (el != NULL) {
      printf("| %d | <-> ", el->data);
      el = el->next;
   }
   printf("NULL\n");
}
