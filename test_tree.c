#include <stdio.h>
#include <stdlib.h>

#include "tree.h"

int main(int argc, char *argv[]) {
    node_t *root = NULL;

    init_tree(&root);

    insert_tree_data(&root, "neco");

    printf("Pre-Order\n--------\n");
    pre_order(root);
    printf("--------\n");

    printf("In-Order\n--------\n");
    in_order(root);
    printf("--------\n");

    printf("Post-Order\n--------\n");
    post_order(root);
    printf("--------\n");

    delete_node(&root);

    return EXIT_SUCCESS;
}
