#ifndef _LIST_H_
#define _LIST_H_

typedef struct SElement {
    struct SElement *next;
    struct SElement *prev;
    int data;
    char *str;
} TElement;

typedef struct SList {
    struct SElement *first;
    struct SElement *last;
    int length;
} TList;

void init_list(struct SList **);
void delete_list(struct SList **);
struct SElement *create_element(int, char *);
void delete_element(struct SElement *);
void insert_first(struct SList *, int, char *);
void insert_last(struct SList *, int, char *);
void delete_first(struct SList *);
void delete_last(struct SList *);
void insert(TList *, TElement *, TElement *);
void append(TList *, TElement *, TElement *);
void delete(struct SList *, struct SElement *);
void copy_list(struct SList *, struct SList *);
TList *merge_list_with_data(TList *first, TList *second, int);
void print_list(struct SList *);

#endif

