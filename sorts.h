#ifndef _SORTS_H_
#define _SORTS_H_

#include "list.h"

TList *insert_sort(TList *);
TList *select_sort(TList *);
TList *quick_sort(TList *);
TList *merge_sort(TList *);

#endif
