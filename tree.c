#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "tree.h"

int _compare_keys(node_t *first, node_t *second) {
    return strcmp(first->data.data, second->data.data);
}

/**** NODE OPERATIONS ****/

node_t *create_node(char *str) {
    node_t *node = (node_t *)malloc(sizeof(node_t));
    int string_length = 0;

    node->weight = 0;
    if (str != NULL) {
        string_length = strlen(str);
        node->data.data = (char *)malloc(sizeof(char)*(string_length + 1));
        memcpy(node->data.data, str, string_length*sizeof(char));
        node->data.data[string_length] = '\0';
    } else {
        node->data.data = NULL;
    }
    node->left = NULL;
    node->right = NULL;

    return node;
}

void delete_node(node_t **node) {
    if ((*node)->data.data != NULL) {
        free((*node)->data.data);
    }

    free((*node));
    *node = NULL;
}


/**** TREE OPERATIONS ****/

void init_tree(node_t **tree) {
    *tree = NULL;
}

void delete_tree(node_t **tree) {
    if (*tree == NULL) {
        return;
    }
    delete_tree(&((*tree)->left));
    delete_tree(&((*tree)->right));
    delete_node(tree);
}


node_t *insert_tree_data(node_t **root, char *data) {
    node_t *new_node = create_node(data);

    insert_tree_node(root, new_node);

    return new_node;
}

void insert_tree_node(node_t **root, node_t *node) {
    if ((*root) == NULL) {
        (*root) = node;
    } else {
    }
}

void delete_tree_node(node_t **root, node_t *node) {
}

node_t *search_tree_string(node_t *root, char *data) {
    return NULL;
}

void pre_order(node_t *root) {
    if (root == NULL) {
        return;
    }
    printf("%s\n", root->data.data);
    pre_order(root->left);
    pre_order(root->right);
}

void in_order(node_t *root) {
    if (root == NULL) {
        return;
    }
    in_order(root->left);
    printf("%s\n", root->data.data);
    in_order(root->right);
}

void post_order(node_t *root) {
    if (root == NULL) {
        return;
    }
    post_order(root->left);
    post_order(root->right);
    printf("%s\n", root->data.data);
}
