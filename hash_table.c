#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "hash_table.h"

void _print_list(TList *);

THashTable *create_hashtable(int size) {
    THashTable *table = NULL;
    TList *tmp = NULL;

    if (size < 1) {
        return NULL;
    }

    table = (THashTable *)malloc(sizeof(THashTable));

    table->table = (TList **)malloc(sizeof(TList *)*size);
    for (int i=0; i<size; i++) {
        tmp = NULL;
        init_list(&tmp);
        table->table[i] = tmp;
    }
    table->size = size;

    return table;
}

unsigned int hash(int size, char *str) {
    unsigned int hashval = 0;
    
    for(; *str != '\0'; str++) {
        hashval = *str + (hashval << 5) - hashval;
    }

    return hashval % size;
}

void hash_input(THashTable *table, unsigned int (*hash_function)(int, char *), char *value) {
    unsigned int pos = hash_function(table->size, value);
    TList *existing = lookup_list(table, hash_function, value);
    TElement *new = NULL;
    char *new_str = NULL;

    if (existing->first != NULL) {
        return;
    }

    new_str = (char *)malloc(sizeof(char)*strlen(value));
    memcpy(new_str, value, sizeof(char)*strlen(value));
    new = create_element(0, new_str);
    existing = table->table[pos];
    append(existing, existing->last, new);
}

TList *lookup_list(THashTable *table, unsigned int (*hash_function)(int, char*), char *value) {
    unsigned int pos = hash_function(table->size, value);
    TList *result = table->table[pos];

    for(TElement *act = result->first; act != NULL; act = act->next) {
        if (strcmp(act->str, value) == 0) {
            break;;
        }
    }

    return result;
}

void delete_table(THashTable **table) {
    if ((*table) == NULL) {
        return;
    }

    for(int i=0; i<(*table)->size; i++) {
        delete_list(&((*table)->table[i]));
    }
    free((*table)->table);
    free((*table));
    *table = NULL;
}

void _print_list(TList *list) {
    if (list->first == NULL) {
        printf("\n");
        return;
    }
    printf("| ");
    for(TElement *act = list->first; act != NULL; act = act->next) {
        printf("%s |", act->str);
    }
    printf("\n");
}

void print_table(THashTable *table) {
    for(int i=0; i<table->size; i++) {
        printf("%d: ", i);
        _print_list(table->table[i]);
    }
}
