# Something smart here

CFLAGS=-std=c99 -pedantic -Wall -g

all: test_sorting test_hashtable test_tree

test_sorting: list.o sorts.o test_sorting.c
	gcc $(CFLAGS) -o test_sorting $^

test_hashtable: list.o hash_table.o test_hashtable.c
	gcc $(CFLAGS) -o test_hashtable $^

test_tree: list.o tree.o test_tree.c
	gcc $(CFLAGS) -o test_tree $^


list.o: list.c
	gcc -c $(CFLAGS) -o $@ $<

sorts.o: sorts.c
	gcc -c $(CFLAGS) -o $@ $<

tree.o: tree.c
	gcc -c $(CFLAGS) -o $@ $<

hash_table.o: hash_table.c
	gcc -c $(CFLAGS) -o $@ $<

clean:
	rm -f test_hashtable test_sorting *.o
