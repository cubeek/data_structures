#include <stdlib.h>

#include "sorts.h"

TList *insert_sort(TList *list) {
    TList *result = NULL;
    TElement *lesser = NULL;
    TElement *new = NULL;

    init_list(&result);

    for (struct SElement *act = list->first; act != NULL; act = act->next) {
        for(lesser = result->first; lesser != NULL && act->data > lesser->data; lesser = lesser->next);
        if (lesser == NULL) {
            insert_last(result, act->data, NULL);
        } else {
            new = create_element(act->data, NULL);
            insert(result, lesser, new);
        }
    }

    return result;
}

TElement *_get_smallest(TList *list) {
    TElement *selected = list->first;
    for(TElement *tmp = list->first; tmp != NULL; tmp = tmp->next) {
        if (selected->data > tmp->data) {
            selected = tmp;
        }
    }

    return selected;
}

TList *select_sort(TList *list) {
    TList *result = NULL;
    TList *tmp = NULL;
    TElement *selected = NULL;

    init_list(&result);
    init_list(&tmp);

    copy_list(list, tmp);

    while (tmp->first != NULL) {
        selected = _get_smallest(tmp);
        delete(tmp, selected);
        append(result, result->last, selected);
    }

    delete_list(&tmp);

    return result;
}

TList *quick_sort(TList *list) {
    TList *result = NULL;
    TList *lesser = NULL;
    TList *greater = NULL;
    TList *lesser_sorted = NULL;
    TList *greater_sorted = NULL;
    TElement *act = NULL, *tmp = NULL;
    TElement *mid = NULL;


    if (list->length == 0 || list->length == 1) {
        init_list(&result);
        copy_list(list, result);
        return result;
    }
    
    act=list->first;
    for (int i=0; i < list->length/2; i++, act = act->next);
    mid = act;

    init_list(&lesser);
    init_list(&greater);

    for (act=list->first; act != NULL; act = act->next) {
        if (act == mid) {
            continue;
        }
        tmp = create_element(act->data, NULL);
        if (act->data < mid->data) {
            append(lesser, lesser->last, tmp);
        } else {
            append(greater, greater->last, tmp);
        }
    }
    
    lesser_sorted = quick_sort(lesser);
    delete_list(&lesser);
    
    greater_sorted = quick_sort(greater);
    delete_list(&greater);

    result = merge_list_with_data(lesser_sorted, greater_sorted, mid->data);
    delete_list(&greater_sorted);
    delete_list(&lesser_sorted);

    return result;
}

TList *_merge_lists(TList *first, TList *second) {
    TList *result = NULL;
    TElement *lesser = NULL;

    init_list(&result);

    while (first->first != NULL || second->first != NULL) {
        if (first->first == NULL) {
            lesser = second->first;
            delete(second, lesser);
        } else if (second->first == NULL || second->first->data > first->first->data) {
            lesser = first->first;
            delete(first, lesser);
        } else {
            lesser = second->first;
            delete(second, lesser);
        }
        append(result, result->last, lesser);
    }

    delete_list(&first);
    delete_list(&second);
    return result;
}

void _split_list(TList *source, TList *first, TList *second) {
    TElement *act = source->first;
    int i;

    for(i=0; i<source->length/2; i++, act = act->next) {
        insert_last(first, act->data, NULL);
    }
    for(; act != NULL; act = act->next) {
        insert_last(second, act->data, NULL);
    }
}

TList *merge_sort(TList *list) {
    TList *result = NULL;
    TList *first = NULL, *second = NULL,
          *first_sorted = NULL, *second_sorted = NULL;

    if (list->length <= 1) {
        init_list(&result);
        copy_list(list, result);
        return result;
    }
   
    init_list(&first);
    init_list(&second); 

    _split_list(list, first, second);

    first_sorted = merge_sort(first);
    second_sorted = merge_sort(second);
    delete_list(&first);
    delete_list(&second);

    result = _merge_lists(first_sorted, second_sorted);

    return result;
}

