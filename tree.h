#ifndef _TREE_H_
#define _TREE_H_

typedef struct _data_t {
    char *data;
} data_t;

typedef struct _node_t {
    int weight;
    data_t data;
    struct _node_t *left;
    struct _node_t *right;
} node_t;


node_t *create_node(char *);
void delete_node(node_t **);

void init_tree(node_t **);
void delete_tree(node_t **);
node_t *insert_tree_data(node_t **, char *);
void insert_tree_node(node_t **, node_t *);
void delete_tree_node(node_t **, node_t *);
node_t *search_tree_string(node_t *, char *);

void pre_order(node_t *);
void in_order(node_t *);
void post_order(node_t *);

#endif
